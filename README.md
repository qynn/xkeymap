
# xkeymap

custom keyboard mapping
  
### xmodmap

Swapping Alt and Win keys:

```
clear mod4
keycode 133 = Alt_L
keycode 134 = Alt_R
add mod4 = Alt_L Alt_R

clear mod1
keycode 64 = Super_L
keycode 108 = Super_R
add mod1 = Super_L Super_R

```
NB: If applied, these changes will be reflected in *output.xkb* when exporting layout with *xkbcomp* (see section below). Applying them at startup is therefore not necessary. 

Using Alt_L as Combination key (`~/.Xmodmap`):
` keysym 64 = Multi_key`

### xkbcomp

from: [X Keyboard extension](https://wiki.archlinux.org/index.php/X_keyboard_extension#Using_keymap)

To get the current configuration:
` xkbcomp $DISPLAY output.xkb `

To test a modified layout:
` test -f map.xkb && xkbcomp map.xkb $DISPLAY `

To apply changes at startup, add to `~/.xinitrc`:
` xkbcomp ~/.Xkeymap $DISPLAY `

### using L_ALT as ISO_Level3_Shift

```
key <LALT> { [ ISO_Level3_Shift ] };

...
interpret ISO_Level3_Lock+AnyOf(all) {
    virtualModifier= LevelThree;
    useModMapMods=level1;
    action= LockMods(modifiers=LevelThree);
};

...
type "THREE_LEVEL" {
    modifiers= Shift+LevelThree;
    map[Shift]= Level2;
    map[LevelThree]= Level3;
    map[Shift+LevelThree]= Level3;
    level_name[Level1]= "Base";
    level_name[Level2]= "Shift";
    level_name[Level3]= "Level3";
};

...
type "FOUR_LEVEL" {
    modifiers= Shift+LevelThree;
    map[Shift]= Level2;
    map[LevelThree]= Level3;
    map[Shift+LevelThree]= Level4;
    level_name[Level1]= "Base";
    level_name[Level2]= "Shift";
    level_name[Level3]= "Alt Base";
    level_name[Level4]= "Shift Alt";
};

...
key <AD01> {
    type= "FOUR_LEVEL",
      symbols[Group1]= [    q,  Q,  1,  exclam ]
};

...
key <AC07> {
  type= "THREE_LEVEL",
  symbols[Group1]= [    j,  J,  Down ]
};

```


### xbindkeys

*xbindkeys* is used for Volume Control. See `xbindkeysrc` for details.

To update config file:
```
xbindkeys --poll-rc
```

### Layout

*xkbcomp* and *xbindkeys* are essentially used to emulate the ![gitlab.com/qynn/levine](https://gitlab.com/qynn/levine/-/blob/master/layout/levine-L34.png) 40% layout.

- <Alt_L> is used as `LVL3`
- <Shift> + <Alt_L>  emulates `LVL4`
- <Alt_L> + b is mapped to BackSpace

Hardcoded [Levine](https://gitlab.com/qynn/Levine) keybindings also rely on Xkeymap for accents and special caracters (e.g. `eacute`, `dead_grave`, unicode `ç`, etc.)

Also worth noting that `Shift-ESC` triggers `CapsLock`

### More

TODO: consider this: [User XKB custimization](https://www.vinc17.net/unix/xkb.en.html): split `xkb_types` and `xkb_symbols` for better config readability.
